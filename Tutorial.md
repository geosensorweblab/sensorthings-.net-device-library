# Tutorial #
This is a tutorial about how to use the SensorThings .NET Device Library. It contains the code of two sample device applications that show how to use the library.
### Requirements and Initial Steps ###
Please follow the "Requirements" and "How to use the library" sections in the README.md file.
### Netduino Plus 2 Setting ###
The two examples presented in this tutorial run on Netduino Plus 2 as a microcontroller device. In order to use Netduino Plus 2 you need to download and install all of its requirements which can be found @ http://www.netduino.com/downloads/.
Before running the device application, you need to connect your Netduino to your PC through an external USB serial adapter, and then you need to connect the Netduino to your Ethernet router through a network cable.
### Sample Applications ###
#### 1. Voltage Reader
This application reads Netduino analog input every 5 seconds and send the reading to the SensorThings API server as a new Observation. The voltage reading is adjusted manually using a potentiometer. In Netduino, the “Analog In 0” was used as low port, “Analog In 2” was used as high port, and “Analog In 1” was used as the voltage port.
```
#!C#
using System.IO;
using System.Net;
using System.Text;
using Microsoft.SPOT;
using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using OGCSensorThingsMicroClassLibrary;


public class VoltageReading
{
    public static void Main()
    {     

        const double maxVoltage = 3.3;
        const int maxAdcValue = 1023;
        var voltagePort = new SecretLabs.NETMF.Hardware.AnalogInput(Pins.GPIO_PIN_A1);
        var lowPort = new OutputPort(Pins.GPIO_PIN_A0, false);
        var highPort = new OutputPort(Pins.GPIO_PIN_A2, true);

        Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");

        var count = 1;
        while (true)
        {
            int rawValue = voltagePort.Read();
            double value = (rawValue * maxVoltage) / maxAdcValue;
            Debug.Print("Count = " + count);
            Debug.Print(rawValue + " " + value.ToString("f"));

            Debug.Print(srvc.addNewObservation(value.ToString("f"), System.DateTime.Now.ToString(), 175186, 196303));

            count++;
            Debug.Print("Sleeping...");
            Thread.Sleep(5000); // 5 sec
        }
    }
}

```
Sample Output:
```
Count = 1
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299435)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299435)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:06.451Z","id":299435,"selfLink":"http://localhost:8080/v1.0/Observations(299435)"}
Sleeping...
Count = 2
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299436)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299436)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:11.743Z","id":299436,"selfLink":"http://localhost:8080/v1.0/Observations(299436)"}
Sleeping...
Count = 3
492 1.59
{"result":"1.59","Datastream":{"navigationLink":"../Observations(299437)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299437)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:17.220Z","id":299437,"selfLink":"http://localhost:8080/v1.0/Observations(299437)"}
Sleeping...
Count = 4
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299438)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299438)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:22.498Z","id":299438,"selfLink":"http://localhost:8080/v1.0/Observations(299438)"}
Sleeping...
Count = 5
492 1.59
{"result":"1.59","Datastream":{"navigationLink":"../Observations(299439)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299439)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:27.785Z","id":299439,"selfLink":"http://localhost:8080/v1.0/Observations(299439)"}
Sleeping...
Count = 6
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299440)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299440)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:33.068Z","id":299440,"selfLink":"http://localhost:8080/v1.0/Observations(299440)"}
Sleeping...
Count = 7
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299441)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299441)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:38.345Z","id":299441,"selfLink":"http://localhost:8080/v1.0/Observations(299441)"}
Sleeping...
Count = 8
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299442)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299442)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:43.631Z","id":299442,"selfLink":"http://localhost:8080/v1.0/Observations(299442)"}
Sleeping...
Count = 9
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299443)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299443)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:48.910Z","id":299443,"selfLink":"http://localhost:8080/v1.0/Observations(299443)"}
Sleeping...
Count = 10
491 1.58
{"result":"1.58","Datastream":{"navigationLink":"../Observations(299444)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299444)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T03:15:54.184Z","id":299444,"selfLink":"http://localhost:8080/v1.0/Observations(299444)"}
Sleeping...


```

#### 3. Sound Reader
This application reads values from sound (microphone) sensor and send some of the readings to the SensorThings API server as new Observations. Every 5 sound readings, the application add the fifth reading value as new observation using the Device Library. The VCC port of the microphone sensor was connected to V5 port in Netduino, while the AUD port was connected to the “Analog In 0”. The GND port was connected to the ground port in Netduino.
```
#!C#
using System.IO;
using System.Net;
using System.Text;
using Microsoft.SPOT;
using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;
using OGCSensorThingsMicroClassLibrary;

public class SoundReader
{
    static SecretLabs.NETMF.Hardware.AnalogInput soundLevel = new SecretLabs.NETMF.Hardware.AnalogInput(Pins.GPIO_PIN_A0);


    public static void Main()
    {
        Service srvc = new Service("http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0");
        var count = 1;

        var reading = soundLevel.Read();
        while (true)
        {

            Debug.Print("Count = " + count);
            Debug.Print(reading + "");


            if (count % 5 == 0)
            {
                Debug.Print(srvc.addNewObservation(reading.ToString(), System.DateTime.Now.ToString(), 299075, 299074));
            }

            reading = soundLevel.Read();
            count++;
        }

    }

}
```
Sample Output:
```
Count = 1
1023
Count = 2
977
Count = 3
982
Count = 4
953
Count = 5
880
{"result":"880","Datastream":{"navigationLink":"../Observations(299378)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299378)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T01:48:18.299Z","id":299378,"selfLink":"http://localhost:8080/v1.0/Observations(299378)"}
Count = 6
806
Count = 7
776
Count = 8
803
Count = 9
645
Count = 10
839
{"result":"839","Datastream":{"navigationLink":"../Observations(299379)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299379)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T01:48:18.582Z","id":299379,"selfLink":"http://localhost:8080/v1.0/Observations(299379)"}
Count = 11
713
Count = 12
692
Count = 13
760
Count = 14
721
Count = 15
736
{"result":"736","Datastream":{"navigationLink":"../Observations(299380)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299380)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T01:48:19.039Z","id":299380,"selfLink":"http://localhost:8080/v1.0/Observations(299380)"}
Count = 16
831
Count = 17
818
Count = 18
764
Count = 19
713
Count = 20
835
{"result":"835","Datastream":{"navigationLink":"../Observations(299381)/Datastream"},"FeatureOfInterest":{"navigationLink":"../Observations(299381)/FeatureOfInterest"},"phenomenonTime":"2015-08-07T01:48:19.328Z","id":299381,"selfLink":"http://localhost:8080/v1.0/Observations(299381)"}
Count = 21
738

```
For further description about each class and method, refer to the SensorThings .NET Device Library Documentation @ http://mro277002.bitbucket.org/SensorThingsDeviceLibraryDoc.




