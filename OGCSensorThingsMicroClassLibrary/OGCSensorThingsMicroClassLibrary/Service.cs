using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.SPOT;
using System.Threading;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;


namespace OGCSensorThingsMicroClassLibrary
{
    /// <summary>
    /// Contains classes that support OGC SensorThings API Sensing Profile services
    /// </summary>
    internal class NamespaceDoc
    {
    }


    /// <summary>
    /// Interacts with the SensorThings API services to CREATE new observation data.
    /// </summary>
    public class Service
    {
        /// <summary>
        /// The URL to the service root that specifies the location of the SensorThings service and the version number.
        /// </summary>
        public string serviceRootURL { get; set; }

        /// <summary>
        /// Creates a new instance of the Service class with the specified service root URL.
        /// </summary>
        /// <param name="serviceRootURL">Referes to the serviceRootURL parameter of the Service</param>
        public Service(string serviceRootURL)
        {
            this.serviceRootURL = serviceRootURL;
        }


        ////****************************** Observations related functions ******************************************
        
        /// <summary>
        /// Create New Observation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="result">The result of the Observation entity.</param>
        /// <param name="phenomenonTime">The result of the Observation entity.</param>
        /// <param name="datastreamID">The id of the Datastream to be associated with the new Observation entity.</param>
        /// <param name="featureOfInterestID">The id of the FeatureOfInterest to be associated with the new Observation entity.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewObservation(string result, string phenomenonTime, long datastreamID, long featureOfInterestID = 0)
        {
            string jsonStr = "{\"result\": " + result + ", \"phenomenonTime\":\"" + phenomenonTime + "\"";

            if (datastreamID > 0)
            {
                jsonStr += ", \"Datastream\":{\"@iot.id\": " + datastreamID + "}";
            }

            if (featureOfInterestID > 0)
            {
                jsonStr += ", \"FeatureOfInterest\":{\"@iot.id\": " + featureOfInterestID + "}";
            }

            jsonStr += "}";



            return addNewObservationByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Observation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="observation">The new Observation entity to be added.</param>
        /// <param name="datastreamID">The id of the Datastream to be associated with the new Observation entity.</param>
        /// <param name="featureOfInterestID">The id of the FeatureOfInterest to be associated with the new Observation entity.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewObservation(Observation observation, long datastreamID, long featureOfInterestID = 0)
        {
            string jsonStr = "{\"result\": " + observation.result + ", \"phenomenonTime\":\"" + observation.phenomenonTime + "\"";

            if (datastreamID > 0)
            {
                jsonStr += ", \"Datastream\":{\"iot.id\": " + datastreamID + "}";
            }

            if (featureOfInterestID > 0)
            {
                jsonStr += ", \"FeatureOfInterest\":{\"iot.id\": " + featureOfInterestID + "}";
            }

            jsonStr += "}";



            return addNewObservationByJSON(jsonStr);
        }

        /// <summary>
        /// Create New Observation entity in OGC SensorThings database.
        /// </summary>
        /// <param name="jsonString">A JSON string describing the new Observation entity to be created.</param>
        /// <returns>JSON string of the new Observation entity or Error message if new record is not added.</returns>
        public string addNewObservationByJSON(string jsonString)
        {
            return addRecord(serviceRootURL + "/Observations", jsonString);
        }

        //***************************************************************************************************


        //***************************** Supportive Functions **********************************************



        /// <summary>
        /// Get the requested IoT entities specified by the given URL.
        /// </summary>
        /// <param name="URL">A URL string specifying the enetities to be returned.</param>
        /// <returns>JSON string representing the returned entities.</returns>
        public string getJson(string URL)
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json; charset=utf-8";

            var response = (HttpWebResponse)request.GetResponse();
            var jsonStr = "";
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                jsonStr = sr.ReadToEnd();
            }

            return jsonStr;
        }

        /// <summary>
        /// Create new entity in the OGC SensorThings database.
        /// </summary>
        /// <param name="URL">String specifying the entity type of the new record.</param>
        /// <param name="json">JSON string describing the new record to be added.</param>
        /// <returns>The status of adding new record.</returns>
        public string addRecord(string URL, string json)
        {
            var request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            byte[] byteArray = Encoding.UTF8.GetBytes(json);
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            string rsultStr = "";

            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    rsultStr = sr.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                return "Error:\n" + ex.Message;
            }


            return rsultStr;
        }
    }
}
