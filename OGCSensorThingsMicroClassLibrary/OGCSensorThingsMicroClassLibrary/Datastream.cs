using System;
using Microsoft.SPOT;
using System.Text;
using System.Collections;

namespace OGCSensorThingsMicroClassLibrary
{
    /// <summary>
    /// Groups a collection of Observations that measure the same ObservedProperty and are produced by the same Sensor.
    /// </summary>
    public class Datastream
    {

        /// <summary>
        /// The system-generated identifier of the Datastream entity. It is unique among entities of the same type in the SensorThings service.
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// The description of the Datastream entity.
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// A JSON Object string that contains three key-value pairs: the name property which represents the full 'name' of the unitOfMeasurment, the 'symbol' property which shows the textual form of the unit symbol, and the 'definition' which contains the IRI defining the unitOfMeasurement.
        /// </summary>
        public string unitOfMeasurement { get; set; }
        /// <summary>
        /// The type of the Observation which is used by the service to encode observations.
        /// </summary>
        public string observationType { get; set; }
        /// <summary>
        /// The spatial bounding box of the spatial extent of all FeaturesOfInterest that belong to the Observations associated with the corresponding Datastream.
        /// </summary>
        public string observedArea { get; set; }
        /// <summary>
        /// The temporal bounding box of the phenomenon times of all observations belonging to the corresponding Datastream.
        /// </summary>
        public DateTime phenomenonTime { get; set; }
        /// <summary>
        /// The temporal bounding box of the result times of all observations belonging to the corresponding Datastream.
        /// </summary>
        public DateTime resultTime { get; set; }
        /// <summary>
        /// The list of Observations of the corresponding Datastream
        /// </summary>
        public ArrayList observations { get; set; }

        /// <summary>
        /// Creates a new instance of the Datastream class with the specified id.
        /// </summary>
        /// <param name="id">Referes to the id parameter of the Datastream</param>
        public Datastream(long id)
        {
            this.id = id;
        }

        /// <summary>
        /// Creates a new instance of the Datastream class with the specified description, unitOfMeasurment, observationType, thing, sensor, observedProperty, observations list, phenomenonTime, and resultTime.
        /// </summary>
        /// <param name="description">Referes to the description parameter of the Datastream</param>
        /// <param name="unitOfMeasurement">Referes to the unitOfMeasurement parameter of the Datastream</param>
        /// <param name="observationType">Referes to the observationType parameter of the Datastream</param>
        /// <param name="observations">Referes to the observations parameter of the Datastream</param>
        /// <param name="observedArea">Referes to the observedArea parameter of the Datastream</param>
        /// <param name="phenomenonTime">Referes to the phenomenonTime parameter of the Datastream</param>
        /// <param name="resultTime">Referes to the resultTime parameter of the Datastream</param>
        public Datastream(string description, string unitOfMeasurement, string observationType,
            ArrayList observations = null,
            string observedArea = "",
            DateTime phenomenonTime = new DateTime(), DateTime resultTime = new DateTime())
        {
            this.description = description;
            this.unitOfMeasurement = unitOfMeasurement;
            this.observationType = observationType;
            this.observations = observations;
            this.observedArea = observedArea;
            this.phenomenonTime = phenomenonTime;
            this.resultTime = resultTime;
        }
    }
}
