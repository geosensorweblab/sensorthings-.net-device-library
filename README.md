# SensorThigs .NET Device Library #

This contains some general information about the SensorThings Device Library, system requirements, and the main steps you need to follow in order to use the library.

### What is this repository for? ###

This device client library was developed using .NET Micro Framework 4.2, and it utilizes the OGC SensorThings API to provide access to .NET system functionality. It can be used to develop applications that run on devices (microcontrollers) as clients, and it is mainly utilized to create new SensorThings observations.

### Requirements ###


* .NET Micro Framework 4.2.
* Microsoft Visual Studio 2010 (or later version). You can use the free Express version.
* The device application should be written in C# language.
* If you would like to use Netduino as a microcontroller device you can download its requirements @ http://www.netduino.com/downloads/

### How to use the library? ###

1. Start new C# Micro Framework windows application in Visual Studio 2010.
2. Right click on the References tab of your application, and then click Add Reference...
3. In the Reference Manager window, click on 'Brows', then navigate to:
OGCSensorThingsMicroClassLibrary > OGCSensorThingsMicroClassLibrary > bin > Debug
4. In the Debug folder add OGCSensorThingsMicroClassLibrary.dll as new references.
5. Add the following in the top of each .cs file where you will use the library classes or methods:

```
#!C#

using OGCSensorThingsMicroClassLibrary;
```
### Documentation ###
Comprehensive documentation about the library classes and methods can be found @ http://mro277002.bitbucket.org/SensorThingsDeviceLibraryDoc.